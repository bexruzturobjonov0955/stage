# Use the official Nginx image as base
FROM nginx:alpine

# Copy our HTML file into the Nginx server's directory
COPY index.html /usr/share/nginx/html

# Expose port 80 to allow external access
EXPOSE 80
